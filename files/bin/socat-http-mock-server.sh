#!/bin/sh
#
# socat -d ssl-l:443,reuseaddr,keepalive,fork,cert=/etc/wireguard.pem,verify=0 system:"./server.sh UDP4\:127.0.0.1\:5757"
# socat -d tcp-l:80,reuseaddr,keepalive,fork system:"./server.sh UDP4\:127.0.0.1\:5757"
# ./server.sh "tcp-l:80,reuseaddr,keepalive,fork" "UDP4:127.0.0.1:5757"
#

if [ $# -eq 1 ]; then
  read -r HTTP
  case $HTTP in
  "GET "*) echo -e "HTTP/1.0 200\nContent-Type: text/plain\n\nHello World!" ;;
  "POST "*) exec socat - "$@" ;;
  *) exec echo -e "HTTP/1.0 500\nContent-Type: text/plain\n\nerror" ;;
  esac
fi

if [ $# -eq 2 ]; then
  exec socat -d "$1" system:"$0 ${2//:/\\:}"
fi
