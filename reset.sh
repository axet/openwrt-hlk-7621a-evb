#!/bin/bash
#
# umount /overlay && jffs2reset && reboot now
#

set -e

HOST=$(awk '/hostname/ {gsub(/'"'"'/, "", $3); print $3}' files/etc/config/system)
: ${BIN:="*-squashfs-sysupgrade.bin"}
: ${LAN:="root@$HOST.lan"}

ssh -C "$LAN" "firstboot -y && reboot"
