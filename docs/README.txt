The HLK-7621A module use the MT7621A chipset.

CPU: MT7621 / 880 MHz / MIPS
Flash: 32MB
RAM: 256MB

The MT7621A integrates aual-core MIPS-1004Kc(880MHz), HNAT/HQoS/Samba/VPN accelerators, 5-portGbE switch, RGMII, USB3.0, USB2.0, 3xPCIe, SD-XC.

The powerful CPU with rich portfolio is suitable for 802.11ac, LTE cat4/5, edge, hotspot, VPN,AC (Access Control).
For the next generation router, MT7621A provides several dedicated hardware engines to accelerate the NAT, QoS, Samba and VPN traffic.These accelerators relief the CPU for other upper layer applications.

Features
 
1.Embedded MIPS1004Kc (880 MHz, Dual-Core)
   32 KB I-Cache and 32 KB D-Cache per core
   256 KB L2 Cache (shared by Dual-Core
   SMP capable
   Single processor operation configurable
 
2.Gigabit Switch
5 ports with full-line rate
5-port 10/100/1000Mbps MDI transceivers
 
3.One RGMII/MII interface
 
4.16-bit DDR2/3 up to 256/512 Mbytes
 
5.SPI(2 chip select), NAND Flash(SLC), SDXC,eMMC(4 bits)
 
6.USB3 x 1+ USB2 x 1 or USB2 x 2 (all host)
 
7.PCIe host x 3
 
8.I2C, UART Lite x 3, JTAG, MDC, MDIO, GPIO
 
9.VoIP support (I2S, PCM)
 
10.Audio interface (SPDIF-Tx, I2S, PCM)
 
11.Deliver the superb Samba performance via USB2.0/USB 3.0/SD-XC
 
12.HW storage accelerator
 
13.HW NAT
    2Gbps wired speed
    L2 bridge
    IPv4 routing, NAT, NAPT
    IPv6 routing, DS-Lite, 6RD, 6to4
 
14.HW QoS
   16 hardware queues to guarantee the min/max bandwidth of each flow.
   Seamlessly co-work with HW NAT engine.
   2Gbps wired speed.
 
15.HW Crypto
 
16.Deliver 400~500 Mbps IPSec throughput
 
17.Green
    Intelligent Clock Scaling (exclusive)
    DDR2/3: ODT off, Self-refresh mode
 
18.Firmware: Linux 2.6 SDK, OpenWRT
 
19.RGMII iNIC Driver: Linux 2.4/2.6


Links

https://www.mediatek.com/products/home-networking/mt7621