#!/bin/bash

IP=10.10.10.3
RT=10.10.10.123
EN=$(ip -4 route ls |awk '/^default/{print $5}')

case $1 in
setup)
  cat << EOF > /etc/network/interfaces.d/eth0
auto eth0:0
allow-hotplug eth0:0
iface eth0:0 inet static
   address $IP
   netmask 255.255.255.0
EOF
  apt install tftpd-hpa
  chmod o+rw /srv/tftp
;;
link)
  ln -sf *-squashfs-sysupgrade.bin 'uImage'
;;
ip)
  sudo ip a add $IP/24 dev $EN
;;
tcpdump)
  sudo tcpdump -n -i $EN host $RT
;;
tftpd)
  [ -e ./tftpd/tftpd ] || (cd tftpd && go get && go build)
  sudo ./tftpd/tftpd
;;
sync)
  rsync -avP --ignore-missing-args *.bin hlk-7621a-uboot/uboot.bin $HOST:/srv/tftp/
;;
esac
