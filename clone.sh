#!/bin/bash

set -e

which apt-get && sudo apt-get install -y libncurses5-dev gawk subversion zlib1g-dev libssl-dev libxml-parser-perl rsync

git clone -b v24.10.0 https://git.openwrt.org/openwrt/openwrt.git trunk

git clone https://gitlab.com/axet/hlk-7621a-uboot
